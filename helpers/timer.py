"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>
  Copyright (C) 2021 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from threading import Timer


class ResetableTimer:
    """
    A threading timer with the ability to reset.
    """

    def __init__(self, interval, function, args=None, kwargs=None):
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.timer = None
        self._new_timer()

    def _new_timer(self):
        self.timer = Timer(
            self.interval, self.function, args=self.args, kwargs=self.kwargs
        )

    def start(self):
        """
        Starts the timer
        """
        self.timer.start()

    def reset(self, start=False, interval=None):
        """
        Resets the timer
        """

        self.cancel()

        if interval:
            self.interval = interval

        self._new_timer()
        if start:
            self.start()

    def cancel(self):
        """
        Cancels the timer
        """
        if self.timer and self.timer.is_alive:
            self.timer.cancel()


def resetable_timer(interval):
    """
    Decorator to create a ResetableTimer instance for the decorated function
    """

    def decorator(func):
        def wrapfn(*args, **kwargs):
            return ResetableTimer(interval, func, args=args, kwargs=kwargs)

        return wrapfn

    return decorator
