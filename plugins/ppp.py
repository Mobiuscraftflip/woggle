"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>
  Copyright (C) 2021 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import random
import time
from enum import IntEnum, auto

import sopel.plugin as plugin

from helpers import auth, timer


class PimPamPetSettings:
    """
    The class (which started out just for the settings) and is now everything PPP related
    """

    ## STRINGS array: Alle the strings with the texts the bot will say during a game

    # Dutch Translations
    STRINGS_NL = {
        "categoryformat": "{} met de letter {}",
        "line": "-----------------------------------------------",
        "nextquestion": "De nieuwe vraag voor PimPamPet is: ",
        "roundsup": "Deze PimPamPet-ronde is voorbij!",
        "timeover": "De tijd is voorbij... Geen antwoorden meer...!",
        "getpoint": "{} krijgt een punt",
        "noticepoints": "Goedzo! Je hebt nu {} punten.",
        "requestpoints": "Lekker bezig, je hebt al {} punten!",
        "notvaliduser": "Dat is geen geldige speler!",
        "negativepoint": "{} krijgt een strafpunt",
        "hiscoreCleared": "De highscores zijn gereset",
        "noaccessclear": "Jij mag de hiscores niet wissen!",
        "unknownchannel": "In welk kanaal?",
        "willStartSoon": "PimPamPet zal hier zo beginnen!",
        "endOfTheGame": "Dit was het voor nu, tot later!",
        "partMsg": "Ik ga even uit rusten",
        "NoGameRunning": "Er is geen spelletje in dit kanaal!",
        "NotInAllowList": "Hier mag ik geen spel opstarten",
        "LowOnPoints": "{} heeft nog geen punten",
        "hiscore": "{} heeft {} punten",
        "NotInChannel": "{} zit niet in dit kanaal",
        "noarg": "Je moet wel een argument opgeven",
        "novalidchannel": "Dat is geen geldig kanaal (Die beginnen met een #)",
        "startingIn": "Ik ga een spel starten in {}",
    }

    # English Translations
    STRINGS_EN = {
        "categoryformat": "{} starting with the letter {}",
        "line": "-----------------------------------------------",
        "nextquestion": "The new question for scattergories is: ",
        "roundsup": "This round of scattergories is done!",
        "timeover": "Time is up... No more answers...!",
        "getpoint": "{} gets a point",
        "noticepoints": "Good job! You got {} points.",
        "requestpoints": "good busy, you have earned {} points so far!",
        "notvaliduser": "That is not a valid player!",
        "negativepoint": "{} gets a punishment point",
        "hiscoreCleared": "The highscores got reset",
        "noaccessclear": "You do not have the permissions to wipe the highscores",
        "unknownchannel": "In what channel?",
        "willStartSoon": "scattergories will start shortly!",
        "endOfTheGame": "That's it for now, till next game!",
        "partMsg": "I'm going to sleep",
        "NOGAMERunning": "There is currently no game to stop in this channel",
        "NotInAllowList": "I'm not allowed to start a game here",
        "LowOnPoints": "{} has no points to deduct",
        "hiscore": "{} has {} points",
        "NotInChannel": "{} is not in the channel",
        "noarg": "You need to supply an argument",
        "novalidchannel": "Thats not a valid channel (Channels start with a #)",
        "startingIn": "I'm starting a game in {}",
    }

    ALPHABET = [
        "A",
        "S",
        "D",
        "F",
        "R",
        "T",
        "G",
        "H",
        "Y",
        "J",
        "U",
        "I",
        "K",
        "L",
        "O",
        "P",
        "Q",
        "A",
        "Z",
        "X",
        "C",
        "V",
        "B",
        "N",
        "M",
        "K",
        "C",
        "H",
        "G",
        "F",
        "E",
        "A",
        "I",
        "U",
        "E",
        "N",
        "T",
        "R",
        "S",
        "E",
        "D",
        "C",
        "F",
        "G",
        "T",
        "H",
        "U",
        "J",
        "I",
        "S",
        "D",
        "R",
        "W",
        "Y",
    ]

    class GameState(IntEnum):
        """
        A enum class to represent the different states of the game
        """

        NOGAME = auto()  # Do nothing
        ASK_QUESTION = auto()  # Generate and ask the Question
        ASKED_QUESTION = auto()  # Wait till the time has come to do the next state
        COOLDOWN = auto()  # Say times up, and go into the cooldown state

    def __init__(self, bot):
        """
        Initialize the class
        """
        # Create the bot variables
        self.bot = bot
        self.channels_allowed = bot.config.ppp.channels_allowed
        self.answer_time = int(bot.config.ppp.answer_time)
        self.cooldown_time = int(bot.config.ppp.cooldown_time)
        self.channel_control = bot.config.ppp.channel_control
        self.lang = bot.config.ppp.lang
        self.game_channels = []
        self.game_started = False
        self.game_timer = None
        self.categories = []

        self.set_language()
        self.load_categories()

    def set_language(self):
        """
        Function to set the language of the bot
        """

        lang_options = {
            "en": {
                "strings": self.STRINGS_EN,
                "categories_file": "./data/ppp/ppp-en.txt",
            },
            "nl": {
                "strings": self.STRINGS_NL,
                "categories_file": "./data/ppp/ppp-nl.txt",
            },
        }

        try:
            self.strings = lang_options[self.lang.lower()]["strings"]
            self.categories_file = lang_options[self.lang.lower()]["categories_file"]
        except KeyError:
            self.strings = lang_options["en"]["strings"]
            self.categories_file = lang_options["en"]["categories_file"]

    def load_categories(self):
        """
        Function to load the categories from the categories file
        """
        self.categories = (
            []
        )  # Reset the categories in case of a reload requested by a user
        with open(self.categories_file, "r") as file:
            for line in file:
                self.categories.append(line.strip())

    ## Function to access the DB, this way its just a bit easier to type in my other functions
    def getdb(self, bot, channel, key, default=None):
        """
        Get the things from the database, a one line function to make it more readable
        Also make it do things, like auto add the ppp_ so its doesn't crash with other values
        """
        return bot.db.get_channel_value(channel, "ppp_{}".format(key), default)

    def setdb(self, bot, channel, key, value):
        """
        Set the things from the database, a one line function to make it more readable
        Also make it do things, like auto add the ppp_ so its doesn't crash with other values
        """
        return bot.db.set_channel_value(channel, "ppp_{}".format(key), value)

    def getdbuser(self, bot, user, key, default=None):
        """
        Get the things from the database, a one line function to make it more readable
        Also make it do things, like auto add the ppp_ so its doesn't crash with other values
        """
        return bot.db.get_nick_value(user, "ppp_{}".format(key), default)

    def setdbuser(self, bot, user, key, value):
        """
        Set the things from the database, a one line function to make it more readable
        Also make it do things, like auto add the ppp_ so its doesn't crash with other values
        """
        return bot.db.set_nick_value(user, "ppp_{}".format(key), value)

    def main_loop(self, bot):
        """
        Main loop of the game. Check every 5 seconds if we need to do something
        """

        # Loop through all the channels
        for channel in bot.channels:
            # If this channel is not active
            if channel not in self.game_channels:
                continue

            # If the game is in cooldown, but the time is over, set the gamestate to Ask Question
            self.state_cooldown(bot, channel)
            self.state_ask_question(bot, channel)
            self.state_asked_question(bot, channel)

    def state_cooldown(self, bot, channel):
        """
        Wait till the cooldown is overs
        """

        if (
            self.GameState(self.getdb(bot, channel, "gameState"))
            is self.GameState.COOLDOWN
        ):
            if self.getdb(bot, channel, "timer") + self.cooldown_time < int(
                time.time()
            ):
                self.setdb(bot, channel, "gameState", self.GameState.ASK_QUESTION)

    def state_ask_question(self, bot, channel):
        """
        Function to ask the question and set the game state to ASKED_QUESTION
        """
        if (
            self.GameState(self.getdb(bot, channel, "gameState"))
            is self.GameState.ASK_QUESTION
        ):
            # Send the begin of the new question sequence
            bot.say(self.strings["line"], channel)
            bot.say(self.strings["nextquestion"], channel)

            # Generate the question
            bot.say(
                self.strings["categoryformat"].format(
                    random.choice(self.categories), random.choice(self.ALPHABET)
                ),
                channel,
            )

            # Send the end question line
            bot.say(self.strings["line"], channel)

            # Set the new states
            self.setdb(bot, channel, "gameState", self.GameState.ASKED_QUESTION)
            self.setdb(bot, channel, "timer", int(time.time()))

    def state_asked_question(self, bot, channel):
        """
        Function to handle the end of the question
        """
        if (
            self.GameState(self.getdb(bot, channel, "gameState"))
            is self.GameState.ASKED_QUESTION
        ):
            # Check if the time is over
            if self.getdb(bot, channel, "timer") + self.answer_time < int(time.time()):
                self.setdb(bot, channel, "gameState", self.GameState.COOLDOWN)
                self.setdb(bot, channel, "timer", int(time.time()))
                bot.say(self.strings["timeover"], channel)

    def start(self, bot, trigger):
        """
        Function to start the game
        """

        # Check if the command came from a management channel
        if trigger.sender not in self.channel_control:
            return

        # Check if there was an argument given
        if not trigger.group(2):
            bot.notice(self.strings["unknownchannel"], trigger.nick)
            return

        if not trigger.group(2)[0:1] == "#":
            bot.notice(self.strings["novalidchannel"], trigger.nick)
            return

        # Check if the bot is allowed in this channel
        if not trigger.group(2) in self.channels_allowed:
            bot.notice(self.strings["NotInAllowList"], trigger.nick)
            return

        # Check if the bot is already in this channel
        if not trigger.group(2) in bot.channels:
            # If not, join it
            bot.join(trigger.group(2))

        # Give the user a notice that we are going to start the game
        bot.notice(self.strings["startingIn"].format(trigger.group(2)), trigger.nick)

        # Set the channel to active
        self.game_channels.append(trigger.group(2))
        self.setdb(bot, trigger.group(2), "gameState", self.GameState.COOLDOWN)
        self.setdb(bot, trigger.group(2), "timer", int(time.time()))
        bot.say(self.strings["willStartSoon"], trigger.group(2))

    def stop(self, bot, trigger):
        """
        Stop the game in a certain channel
        """
        if trigger.group(2) not in self.game_channels:
            bot.notice(self.strings["NoGameRunning"], trigger.nick)
            return

        # Set the channel to inactive
        self.game_channels.remove(trigger.group(2))

        # Mention in the channel that the game is over
        bot.say(self.strings["endOfTheGame"], trigger.group(2))

        # Leave the channel
        bot.part(trigger.group(2), self.strings["partMsg"])

    def award(self, bot, trigger):
        """
        Award the user for a "correct" answer
        """

        # Save the triggers in variables to make it more readable
        user = trigger.group(4)
        channel = trigger.group(3)

        # Check if the user is in the list of users
        if user not in self.get_user_list(bot, channel):
            bot.notice(self.strings["NotInChannel"].format(user), trigger.nick)
            return

        # Get the users current point amount
        current_points = self.getdbuser(bot, user, "points_{}".format(channel), 0)

        # Cause python isn't as fexible as everone says
        if current_points is None:
            current_points = 0

        # Add the points to the users point amount
        current_points = current_points + 1

        # Save the new points amount
        self.setdbuser(bot, user, "points_{}".format(channel), current_points)

        # Send a message to the channel
        bot.say(self.strings["getpoint"].format(user, current_points), channel)
        bot.notice(self.strings["noticepoints"].format(current_points), user)

    def deduct(self, bot, trigger):
        """
        Deduct a point for a "wrong" answer
        """
        # Save the triggers in variables to make it more readable
        user = trigger.group(4)
        channel = trigger.group(3)

        # Check if the user is in the list of users
        if user not in self.get_user_list(bot, channel):
            bot.notice(self.strings["NotInChannel"].format(user), trigger.nick)
            return

        # Get the users current point amount
        current_points = self.getdbuser(bot, user, "points_{}".format(channel), 0)

        # Cause python isn't as fexible as everone says
        if current_points is None:
            current_points = 0

        # Subtract the points from the users point amount
        if current_points >= 0:
            current_points -= 1
        else:
            bot.notice(self.strings["LowOnPoints"].format(user), trigger.nick)

        # Save the new points amount
        self.setdbuser(bot, user, "points_{}".format(channel), current_points)

        # Send a message to the channel
        bot.say(self.strings["negativepoint"].format(user, current_points), channel)

    def get_points(self, bot, trigger):
        """
        Function for the user to get the amount of points
        """

        # Get the users current point amount
        current_points = self.getdbuser(
            bot, trigger.nick, "points_{}".format(trigger.sender), 0
        )

        # Cause python isn't as fexible as everone says
        if current_points is None:
            current_points = 0

        # Send a message to the channel
        bot.notice(self.strings["requestpoints"].format(current_points), trigger.nick)

    def get_highscore(self, bot, trigger):
        """
        Create the list with scores, sort them, and push them in the channel
        """

        highscore_list = []

        for name in self.get_user_list(bot, trigger.sender):
            if self.getdbuser(bot, name, "points_{}".format(trigger.sender), 0) != 0:
                highscore_list.append(
                    {
                        "points": self.getdbuser(
                            bot, name, "points_{}".format(trigger.sender), 0
                        ),
                        "name": name,
                    }
                )

        # Sort the list
        highscore_list = sorted(highscore_list, key=lambda k: k["points"], reverse=True)

        # Send the message to the channel
        for entry in highscore_list:
            bot.say(
                entry["name"] + ": " + str(entry["points"]),
                trigger.sender,
            )

    def get_user_list(self, bot, channel):
        """
        Get the whole list of users in the channel
        """
        user_list = []
        flag_list = []

        # Go through all the users in the channel
        for name, flag in bot.channels[channel].privileges.items():
            # Add it to the list
            user_list.append(name)
            flag_list.append(flag)

        return user_list


def setup(bot):
    """
    Initialize the bot
    """
    bot.memory["pim_pam_pet_settings"] = PimPamPetSettings(bot)


@timer.resetable_timer(5)
def main_loop(bot):
    """
    Run the main loop of the game
    """
    bot.memory["pim_pam_pet_settings"].main_loop(bot)
    main_loop(bot).reset(start=True)


@plugin.commands("pppstart")
@auth.require_permission("ppp")
def start(bot, trigger):
    """
    Start the game
    """
    bot.memory["pim_pam_pet_settings"].start(bot, trigger)
    main_loop(bot).reset(start=True)


@plugin.commands("pppstop")
@auth.require_permission("ppp")
def stop(bot, trigger):
    """
    Stop the game
    """
    bot.memory["pim_pam_pet_settings"].stop(bot, trigger)


@plugin.commands("pppaward")
@auth.require_permission("ppp")
def award(bot, trigger):
    """
    Award a point to the user
    """
    bot.memory["pim_pam_pet_settings"].award(bot, trigger)


@plugin.commands("pppdeduct")
@auth.require_permission("ppp")
def deduct(bot, trigger):
    """
    Deduct a point from the user
    """
    bot.memory["pim_pam_pet_settings"].deduct(bot, trigger)


@plugin.commands("ppphighscore")
def highscore(bot, trigger):
    """
    Get the highscore of the channel
    """
    bot.memory["pim_pam_pet_settings"].get_highscore(bot, trigger)


@plugin.commands("ppppoints", "pppoints")
def ppppoints(bot, trigger):
    """
    Get the amount of points of the user
    """
    bot.memory["pim_pam_pet_settings"].get_points(bot, trigger)
