"""
Woggle - bot framework

  Copyright (C) 2024 default <coder@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin
import random
import re

class Redirector:
    """
    
    
    """

@plugin.event("JOIN")
@plugin.rule(".*")
def redirect(bot, trigger):
    print("Hello new user")
    current_channel = trigger.sender
    user_nick = trigger.nick

    # Extract the prefix from the current channel (everything before the last digit or underscore)
    match = re.match(r'^(.*?)[\d_]*$', current_channel)
    if match:
        prefix = match.group(1)
        
        # Find all channels that match the prefix and end in a number
        available_channels = []
        for chan in bot.channels:
            if re.match(f'^{re.escape(prefix)}\\d+$', chan):
                # Check if the channel is muted (+m)
                if not bot.channels[chan].modes.get('m', False) and len(channel_info.users) <= bot.config.redirect.channellimit:
                    available_channels.append(chan)
        
        # If there are no channels with less then the configured amount of users, select one at random from all the channels.
        if len(available_channels) == 0:
            # TODO: Send out warning to ops channel!
            
            for chan in bot.channels:
                if re.match(f'^{re.escape(prefix)}\\d+$', chan):
                    # Check if the channel is muted (+m)
                    if not bot.channels[chan].modes.get('m', False):
                        available_channels.append(chan)
        
        if available_channels:
            # Randomly select one of the matching channels
            new_channel = random.choice(available_channels)
            
            # Send a notice to the user redirecting them to the new channel
            bot.say(f"Hello {user_nick}, consider joining {new_channel} instead!", trigger.sender)